NAME = "tetriminos-generator"

$(NAME):
	gcc -Wall -Wextra -Werror -std=c99 main.c -o $(NAME)
